// package percolation;

/**
 * PercolationStats implementation for 1st programming assignment of Coursera Princeton University Algorithms, Part I course
 * This solution suffers from backwash problem.
 * Specifications: http://coursera.cs.princeton.edu/algs4/assignments/percolation.html
 * @author Pawel Czarnota
 * @version April 21, 2017
 */

import edu.princeton.cs.algs4.WeightedQuickUnionUF;
import edu.princeton.cs.algs4.StdOut;

public class Percolation {

    private int gridSize;               // store size of our grid
    private boolean[][] isOpen;         // data representation of a boolean grid of open sites
    private int virtualTop;             // virtual top
    private int virtualBottom;          // virtual bottom
    private WeightedQuickUnionUF uf;    // Weighted Quick Union Find object
    private int numberOfOpenSites;      // stores count of open sites
    
    /**
     * create n-by-n grid, with all sites blocked
     * @param n size of the N x N grid 
     */
    public Percolation(int n) {
        if (n <= 0) {
            throw new IllegalArgumentException("argument n out of bounds");
        }
        gridSize = n;
        isOpen = new boolean[gridSize][gridSize];   // initialize to false (blocked)
        numberOfOpenSites = 0;
        virtualTop = 0;
        virtualBottom = gridSize * gridSize + 1;
        uf = new WeightedQuickUnionUF(gridSize * gridSize + 2);
    }
    
    /**
     * open site (row, col) if it is not open already
     * @param row row in the grid
     * @param col column in the grid
     */
    public void open(int row, int col) {
        checkBounds(row, col);
        if (!isOpen[row-1][col-1]) {
            isOpen[row-1][col-1] = true;    // update grid
            numberOfOpenSites++;

            // connect entries located on the top row with our virtual top
            if (row == 1) {
                uf.union(convert2dto1d(row, col), virtualTop);
            }

            // connect entries located on the bottom row with our virtual top
            if (row == gridSize) {
                uf.union(convert2dto1d(row, col), virtualBottom);
            }

            // next cases are for sites located between rows 2 and gridSize - 1

            // connect if the site with previous row and same column is also connected
            // i.e. check site directly above current site
            if (row > 1 && isOpen(row-1, col)) {
                uf.union(convert2dto1d(row, col), convert2dto1d(row-1, col));
            }

            // connect if the site with same row and next column is also connected
            // i.e. check site directly to the left of current site
            if (col < gridSize && isOpen(row, col+1)) {
                uf.union(convert2dto1d(row, col), convert2dto1d(row, col+1));
            }

            // connect if the site with next row and same column is also connected
            // i.e. check site directly below current site
            if (row < gridSize && isOpen(row+1, col)) {
                uf.union(convert2dto1d(row, col), convert2dto1d(row+1, col));
            }

            // connect if the site with same row and previous column is also connected
            // i.e. check site directly to the left of current site
            if (col > 1 && isOpen(row, col-1)) {
                uf.union(convert2dto1d(row, col), convert2dto1d(row, col-1));
            }
        }
    }
    
    /**
     * is site (row, col) open?
     * @param row row in the grid
     * @param col column in the grid
     * @return true if site is open, false if it's not
     */
    public boolean isOpen(int row, int col) { 
        checkBounds(row, col);
        return isOpen[row-1][col-1];    // get information from grid
    }
    
    /**
     * is site (row, col) full?
     * idea here is to return if site is connected to top since flow starts at
     *  the top
     * @param row row in the grid
     * @param col column in the grid
     * @return true if site is full, false otherwise
     */
    public boolean isFull(int row, int col) {  
        checkBounds(row, col);
        return uf.connected(virtualTop, convert2dto1d(row, col));
    }
    
    /**
     * how many sites are open
     * @return number of sites that are open
     */
    public int numberOfOpenSites() {
        return numberOfOpenSites;
    }
    
    /**
     * does the system percolate?
     * @return true if system percolates, false otherwise
     */
    public boolean percolates() {              
        return uf.connected(virtualTop, virtualBottom);
    }
    
    /**
     * Helper function, convert 2D matrix to 1d union-find structure
     * @param x row coordinate in the grid
     * @param y column coordinate in the grid
     * @return converted coordinates to an index in the union-find structure
     */
    private int convert2dto1d(int x, int y) {
        
        return gridSize * (x - 1) + y;
    }
    
    /**
     * Helper function, check if x or y are out of bounds
     * @param x row coordinate in the grid
     * @param y column coordinate in the grid
     */
    private void checkBounds(int x, int y) {
        if (x <= 0 || x > gridSize || y <= 0 || y > gridSize) {
            throw new IndexOutOfBoundsException("row or column index out of bounds");
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // not used, instead use the visualizer class to test
        
        // the following were used during debugging
        // Percolation perc = new Percolation(6);
        // StdOut.println(perc.percolates());
        // perc.open(1, 6);
        // StdOut.println(perc.isOpen(1, 6));
        // StdOut.println(perc.percolates());
    }
    
}
