// package percolation;

import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;
import edu.princeton.cs.algs4.StdOut;

/**
 * PercolationStats implementation for 1st programming assignment of Coursera Princeton University Algorithms, Part I course
 * @author Pawel Czarnota
 * @version April 21, 2017
 */
public class PercolationStats {

   private double[] statsArray;     // stores stats on open sites
   private int trials;                  // store # of trials - needed to calculate 95% confidence
   
   /**
    * perform trials independent experiments on an n-by-n grid
    * @param n size of the n-by-n grid
    * @param trials number of experiments to run
    */
   public PercolationStats(int n, int trials) {
       if (n <= 0 || trials <= 0) {
           throw new IllegalArgumentException("argument N or T not valid");
       }
       this.trials = trials;
       
       statsArray = new double[trials];
       
       // for each trial, perform the experiment of opening sites at random until system percolates
       for (int i = 0; i < trials; i++) {
           
           Percolation perc = new Percolation(n);
           int numberOfOpenSites = 0;
           
           // open sites until system percolates
           while (!perc.percolates()) { 
               int row = StdRandom.uniform(1, n+1); // need to be n+1, with n program enters infinite loop since the system never percolates
               int col = StdRandom.uniform(1, n+1); // need to be n+1, with n program enters infinite loop since the system never percolates
               
               if (!perc.isOpen(row, col)) {
                   perc.open(row, col);
                   numberOfOpenSites++;
                   // StdOut.println("Site:" + row + " " + col + " opened" );
               }
               // StdOut.print("Trial #" + i + " number of open sites:");
               // StdOut.println(numberOfOpenSites);
           }
           
           // calculate stats on open sites for each trial
           statsArray[i] = (double) numberOfOpenSites / (n*n);  // store number of open sites 
       }
   }
   
   /**
    * sample mean of percolation threshold - calculated using StdStats class functions
    * @return computed mean of all trial experiments 
    */
   public double mean() { 

       return StdStats.mean(statsArray);
   }
   
   /**
    * sample standard deviation of percolation threshold
    * @return computed standard deviation of all trial experiments
    */
   public double stddev() {
       
       return StdStats.stddev(statsArray);
   }
   
   /**
    * low endpoint of 95% confidence interval
    *  use formula  from specification: http://coursera.cs.princeton.edu/algs4/assignments/percolation.html
    *  [x - (1.96*s)/sqrt(t) , x + (1.96*s)/sqrt(t)
    *  Another reference: http://onlinestatbook.com/2/estimation/mean.html
    * @return calculated low endpoint of 95% confidence interval
    */
   public double confidenceLo() {                  // 
       
       return this.mean() - (1.96 * this.stddev())/ Math.sqrt(this.trials);
   }
   
   /**
    * high endpoint of 95% confidence interval
    *  use formula  from specification: http://coursera.cs.princeton.edu/algs4/assignments/percolation.html
    *  [x + (1.96*s)/sqrt(t) , x + (1.96*s)/sqrt(t)
    *  Another reference: http://onlinestatbook.com/2/estimation/mean.html
    * @return calculated high endpoint of 95% confidence interval
    */
   public double confidenceHi() {
       
       return this.mean() + (1.96 * this.stddev())/ Math.sqrt(this.trials);
   }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        int trials = Integer.parseInt(args[1]);
        
        PercolationStats percStats = new PercolationStats(n, trials);
        
        StdOut.println("mean                    = " + percStats.mean());
        StdOut.println("stddev                  = " + percStats.stddev());
        StdOut.println("95% confidence interval = [" + percStats.confidenceLo() + ", " + percStats.confidenceHi() + "]");
    }
    
}
